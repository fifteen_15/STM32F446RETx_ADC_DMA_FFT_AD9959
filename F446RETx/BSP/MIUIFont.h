#ifndef __MIUIFONT_H_
#define __MIUIFONT_H_

#include "SystemOS.h"


#define             CharactersNumber            0

typedef struct CheseHZ
{
    const unsigned char Guide[2];
    const unsigned char Code[32];
}__CharactersTypeDef;

extern __CharactersTypeDef Characters[CharactersNumber];





#endif
