#include "MIUI.h"


__MIUIControl_Struct _MIUIControl;
__MIUIDisplay_Struct _MIUIDisplay[MIUINumber] =
{
        {0, _KeySwitch_Close, TaskDisplay_MotorFunction},
};

/******************************************************
 *                      函数重要变量
 ******************************************************/
int **DataSprtCorrect;
uint8_t LoopCount;
uint8_t PageStare;


/******************************************************
 *                      参数 数据修改
 ******************************************************/

typedef enum
{
	MIUI_2 = 0,
	
}_MIUIsort;


int _Guide = 0;
int StartFlag = 1;


#define         ParamMotorNumber        20
__ModeAlter    _MotorAlter[ParamMotorNumber] =
{
        { "StartUp", 	&StartFlag, 		1	},
        { "PhaDiff", 	&AD9959_Pha_Diff, 	5	},
        { "ALLFix", 	&FreqOutFixAll, 	1	},
        { "Fix_20", 	&EverFix[0], 		1	},
        { "Fix_25", 	&EverFix[1], 		1	},
        { "Fix_30", 	&EverFix[2], 		1	},
        { "Fix_35", 	&EverFix[3], 		1	},
        { "Fix_40", 	&EverFix[4], 		1	},
        { "Fix_45", 	&EverFix[5], 		1	},
        { "Fix_50", 	&EverFix[6], 		1	},
        { "Fix_55", 	&EverFix[7], 		1	},
        { "Fix_60", 	&EverFix[8], 		1	},
        { "Fix_65", 	&EverFix[9], 		1	},
        { "Fix_70", 	&EverFix[10], 		1	},
        { "Fix_75", 	&EverFix[11], 		1	},
        { "Fix_80", 	&EverFix[12], 		1	},
        { "Fix_85", 	&EverFix[13], 		1	},
        { "Fix_90", 	&EverFix[14], 		1	},
        { "Fix_95", 	&EverFix[15], 		1	},
        { "Fix_100", 	&EverFix[16], 		1	},
};




void TaskDisplay_MotorFunction(void)
{
    if(_MIUIControl.MIUIControl_Refresh == _MIUIDisplay_Enable)
    {
          switch(_MIUIControl.KeyStatus_StoreData)
          {
              case _KeyStore_KEYUp:
                  if(_MIUIDisplay[MIUI_2].SelectData == _KeySwitch_Open)
                  {
                      DataSprtCorrect = &_MotorAlter[_MIUIDisplay[MIUI_2].TaskSerial].Paramtersprt;
                      **DataSprtCorrect += _MotorAlter[_MIUIDisplay[MIUI_2].TaskSerial].Stepping;
                  }
                  else
                  {
					ips114_clear(BLACK);
                      if((_MIUIDisplay[MIUI_2].TaskSerial) > 0)
                      {
                          _MIUIDisplay[MIUI_2].TaskSerial--;
                      }
                  }
                  break;
              case _KeyStore_KEYDown:
                  if(_MIUIDisplay[MIUI_2].SelectData == _KeySwitch_Open)
                  {
                      DataSprtCorrect = &_MotorAlter[_MIUIDisplay[MIUI_2].TaskSerial].Paramtersprt;
                      **DataSprtCorrect -= _MotorAlter[_MIUIDisplay[MIUI_2].TaskSerial].Stepping;
                  }
                  else
                  {
					ips114_clear(BLACK);
                      if((++_MIUIDisplay[MIUI_2].TaskSerial) > ParamMotorNumber - 1)
                      {
                          _MIUIDisplay[MIUI_2].TaskSerial = ParamMotorNumber - 1;
                      }
                  }
                  break;
              case _KeyStore_KEYLeft:
                  if(_MIUIDisplay[MIUI_2].SelectData == _KeySwitch_Open)
                  {
                      _MIUIDisplay[MIUI_2].SelectData = _KeySwitch_Close;
                  }
                  else
                  {
					  ips114_clear(DSP_BLACK);
//                      _MIUIControl.KeyStatus_CurrentTask = 0;
                      _MIUIControl.MIUIControl_Refresh = _MIUIDisplay_Enable;
                  }
                  break;
              case _KeyStore_KEYRight:
                  //选择该参数 确定修改该参数
                  _MIUIDisplay[MIUI_2].SelectData = _KeySwitch_Open;
                  break;
          }
          _MIUIControl.KeyStatus_StoreData = _KeyStore_NULL;
        if(_MIUIControl.KeyStatus_CurrentTask == MIUI_2)
        {
            _MIUIControl.MIUIControl_Refresh = _MIUIDisplay_Reset;
            DisplayPage_MIUIFunction(MIUI_2, ParamMotorNumber, _MotorAlter);
        }
		
		
		Task_StartupFunction(StartFlag);
    }
}



//
void InitalParameter_MIUIControl(void)
{
	OpenMIUIFunction;
	
    //控制状态初始化
    _MIUIControl.KeyStatus_Present		= 	_KeyStatus_Reset;
    _MIUIControl.KeyStatus_MIUIDisplay	= 	_MIUIDisplay_Open;
    _MIUIControl.KeyStatus_StoreData	= 	_KeyStore_NULL;
    _MIUIControl.MIUIControl_Enable		=	_MIUIControl_Enable;
    _MIUIControl.MIUIControl_Refresh 	= 	_MIUIDisplay_Enable;

    _MIUIControl.KeyStatus_CurrentTask	= 	MIUI_2;

}

//
void BaseConfigure_MIUIFunction(void)//2ms
{
	//定时器初始化
	
}
//
void TaskImplement_MIUIFunction(void)//放入上面开启的定时器中断内
{
    switch(_MIUIControl.KeyStatus_Present)
    {
        case _KeyStatus_Reset:
            if(KEY_Up == GPIO_LOW || KEY_Down == GPIO_LOW || KEY_Left == GPIO_LOW || KEY_Right == GPIO_LOW)
            {
                _MIUIControl.KeyStatus_Present = _KeyStatus_Chick;
            }
            break;
        case _KeyStatus_Chick:
            if(KEY_Up == GPIO_LOW || KEY_Down == GPIO_LOW || KEY_Left == GPIO_LOW || KEY_Right == GPIO_LOW)
            {
                _MIUIControl.KeyStatus_Present = _KeyStatus_Enable;
            }
            else
            {
                _MIUIControl.KeyStatus_Present = _KeyStatus_Reset;
            }
            break;
        case _KeyStatus_Enable:

            _MIUIControl.MIUIControl_ModelCnt = _KEY_CntClear;
            _MIUIControl.MIUIControl_Refresh  = _MIUIDisplay_Enable;
            _MIUIControl.KeyStatus_Present   = _KeyStatus_Clear;

            if(KEY_Up == GPIO_LOW)
            {
                _MIUIControl.KeyStatus_StoreData = _KeyStore_KEYUp;
            }
            else if(KEY_Down == GPIO_LOW)
            {
                _MIUIControl.KeyStatus_StoreData = _KeyStore_KEYDown;
            }
            else if(KEY_Left == GPIO_LOW)
            {
                _MIUIControl.KeyStatus_StoreData = _KeyStore_KEYLeft;
				if(_MIUIControl.MIUIControl_Enable == _MIUIControl_Disable)
				{
					_MIUIControl.MIUIControl_Enable = _MIUIControl_Enable;
					StartFlag = 0;
					Task_StartFlag = 0;
				}
            }
            else if(KEY_Right == GPIO_LOW)
            {
                _MIUIControl.KeyStatus_StoreData = _KeyStore_KEYRight;
            }
            else
            {
                _MIUIControl.KeyStatus_StoreData = _KeyStore_NULL;
                _MIUIControl.KeyStatus_Present   = _KeyStatus_Reset;
                _MIUIControl.MIUIControl_Refresh  = _MIUIDisplay_Reset;
            }
            _MIUIControl.MIUIControl_LongData = _MIUIControl.KeyStatus_StoreData;
            break;
        case _KeyStatus_Clear:
            if(KEY_Up == GPIO_HIGH && KEY_Down == GPIO_HIGH && KEY_Left == GPIO_HIGH && KEY_Right == GPIO_HIGH && KEY_Enable == GPIO_HIGH && KEY_Extra == GPIO_HIGH)
            {
                _MIUIControl.KeyStatus_Present = _KeyStatus_Reset;

                //长按模式清零
                _MIUIControl.MIUIControl_LongData = _KeyStore_NULL;
                _MIUIControl.MIUIControl_ModelCnt = _KEY_CntClear;
            }
            else
            {
                //长按模式实现
                if(_MIUIControl.MIUIControl_ModelCnt > _KEY_LongMode)
                {
                    _MIUIControl.KeyStatus_StoreData = _MIUIControl.MIUIControl_LongData;
                    _MIUIControl.MIUIControl_Refresh  = _MIUIDisplay_Enable;
                }
                else
                {
                    _MIUIControl.MIUIControl_ModelCnt++;
                }
            }
            break;
    }
}
//
//
void DisplayPage_MIUIFunction(unsigned char TaskFunction, unsigned char TaskParamNumber, __ModeAlter TaskAlter[])
{
    PageStare = _MIUIDisplay[TaskFunction].TaskSerial - _MIUIDisplay[TaskFunction].TaskSerial % 6;
    LoopCount = PageStare - 1;

//    MIUI_Clear;
    while((++LoopCount) < PageStare + 6 && LoopCount < TaskParamNumber)
    {
        DataSprtCorrect = &(TaskAlter[LoopCount].Paramtersprt);
        if(LoopCount == _MIUIDisplay[TaskFunction].TaskSerial)
        {
			if(_MIUIDisplay[MIUI_2].SelectData == _KeySwitch_Open)
			{
				ips114_set_color(DSP_RED,DSP_BLACK);
				ips114_show_int (160, (LoopCount % 6) * 20, **DataSprtCorrect, 4);
				ips114_set_color(DSP_PINK,DSP_BLACK);
			}
			else
			{
				ips114_set_color(DSP_PINK,DSP_BLACK);
				ips114_show_int (160, (LoopCount % 6) * 20, **DataSprtCorrect, 4);
			}
//			GuideChese_DSPFunction(20, LoopCount * 20, DSP_PINK, DSP_BLACK, "               ");
			GuideChese_DSPFunction(30,(LoopCount % 6) * 20, DSP_RED, DSP_BLACK, (Uint8*)TaskAlter[LoopCount].ParamterName);
        }
        else
        {
			ips114_set_color(DSP_PINK,DSP_BLACK);
			ips114_show_int (160, (LoopCount % 6) * 20, **DataSprtCorrect, 4);
//			GuideChese_DSPFunction(20, LoopCount * 20, DSP_PINK, DSP_BLACK, "               ");
			GuideChese_DSPFunction(20,(LoopCount % 6) * 20, DSP_PINK, DSP_BLACK, (Uint8*)TaskAlter[LoopCount].ParamterName);
        }
		
		
    }
}
//








