#ifndef __WAVE_H__
#define __WAVE_H__

#include "SystemOS.h"

#define		CH0_SinOut		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_RESET)
#define		CH0_TriOut		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_SET)
#define		CH1_SinOut		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_RESET)
#define		CH1_TriOut		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_SET)

	

extern uint32_t	FreqFix[2];
extern int 		EverFix[17];
extern int 		FreqOutFixAll;
extern int 		AD9959_Pha_Diff;
extern uint8_t	FFT_Complete_Flag;
extern uint32_t WaveFFT_AB[4];
extern uint8_t 	Task_StartFlag;
extern uint32_t WaveFFT_A, WaveFFT_B ;

	

void WavePhaseFix(void);

void WaveSinOrTriOut(void);

void WaveInOutputInital(void);

void WaveInOutputOperation(void);

void Task_StartupFunction(int mod);

uint16_t FreqApproximateFive(uint16_t num);

/*
 * 函数1  初始化FFT运算相关参数
 *	fftLen 			用于指定 FFT长度（16/64/256/1024/4096）本章设置为1024
 *	ifftFlag 		用于指定是傅里叶变换(0)还是反傅里叶变换(1) 本章设置为0
 *	bitReverseFlag  用于设置是否按位取反 本章设置为 1
 * */
arm_status arm_cfft_radix4_init_f32(
	arm_cfft_radix4_instance_f32 * S,
	uint16_t fftLen,uint8_t ifftFlag,uint8_t bitReverseFlag);


/*
 *  函数2  执行基 4 浮点FFT运算
 * 	S结构体指针参数 先由 arm_cfft_radix4_init_f32 函数设置好 然后传入该函数的
 * 	pSrc 		  传入采集到的输入信号数据（实部+虚部形式）同时FFT变换后的数据也按顺序存放在pSrc里面pSrc 				  必须大于等于2倍fftLen长度
 * */
void arm_cfft_radix4_f32(const arm_cfft_radix4_instance_f32 * S,float32_t * pSrc);


/*
 *  函数3  计算复数模值 对 FFT 变换后的结果数据，执行取模操作
 *	pSrc 		复数输入数组（大小为 2*numSamples）指针，指向 FFT 变换后的结果
 *	pDst		输出数组（大小为 numSamples）指针，存储取模后的值
 *	numSamples  就是总共有多少个数据需要取模
 * */
void arm_cmplx_mag_f32(float32_t * pSrc,float32_t * pDst,uint32_t numSamples);



#endif

















/*
*2023.08.02
*Fifteen 
*1579985949@qq.com
*/







