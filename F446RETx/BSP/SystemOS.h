






#ifndef __SystemOS_H_
#define __SystemOS_H_


#define 	BEEP_ON		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_9,GPIO_PIN_RESET)
#define 	BEEP_OFF	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_9,GPIO_PIN_SET)


/*定义各种类型(如果冲突,自行去掉)*/
typedef unsigned char       Uint8;
typedef signed   char       Int8;
typedef unsigned short      Uint16;
typedef signed   short      Int16;
typedef unsigned long       Uint32;
typedef signed   long       Int32;
typedef unsigned long long  Uint64;
typedef signed   long long  Int64;
typedef float               F32;
typedef double              F64;

typedef unsigned char       uint8;
typedef signed   char       int8;
typedef unsigned short      uint16;
typedef signed   short      int16;
typedef unsigned long       uint32;
typedef signed   long       int32;
typedef unsigned long long  uint64;
typedef signed   long long  int64;
typedef float               f32;
typedef double              f64;
/*****************************
 *
 *          底层配置
 *
 ****************************/
#include "stm32f4xx_hal.h"

#include "math.h"
#include "stdio.h"
#include "stdarg.h"
#include "string.h"

#include "main.h"
#include "adc.h"
//#include "dac.h"
#include "dma.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

#include "arm_math.h"

/*****************************
 *
 *         算法控制
 *
 ****************************/

#include "wave.h"
#include "AD9959.h"

/*****************************
 *
 *          外设处理
 *
 ****************************/
 
#include "MIUI.h"
#include "MIUIFont.h"
 
#include "zf_common_font.h"
#include "zf_device_ips114.h"
#include "zf_common_function.h"

/*****************************
 *
 *          用户函数
 *
 ****************************/

void OS_OperationInital(void);
void OS_OperationSystem(void);

#endif








/*
*2023.08.02
*Fifteen 
*1579985949@qq.com
*/







