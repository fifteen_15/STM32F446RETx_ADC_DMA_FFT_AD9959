#ifndef __MIUI_H_
#define __MIUI_H_

#include "SystemOS.h"

#define 	OpenMIUIFunction	HAL_TIM_Base_Start_IT(&htim4)
#define		CloseMIUIFunction	HAL_TIM_Base_Stop_IT(&htim4)

#define		MIUINumber			2

#define		GPIO_LOW			0
#define		GPIO_HIGH			1

#define		KEY_Up				HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4)	//数据上	//移动上	//W
#define		KEY_Down			HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_5)	//数据下	//移动下	//S
#define		KEY_Left			HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_6)	//数据左	//退出		//A
#define		KEY_Right			HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_7)	//数据右	//进入		//D
#define		KEY_Enable			1//HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_5)	//确定		//发车		//E
#define		KEY_Extra			1//HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_6) //

#define		_KEY_CntClear		0
#define		_KEY_LongMode		15

//最多支持250个参数
#define		ParamMaxNumber		99

//Colors define
#define		DSP_WHITE           0xFFFF  // 白色
#define 	DSP_BLACK           0x0000  // 黑色
#define 	DSP_BLUE            0x001F  // 蓝色
#define 	DSP_PURPLE          0XF81F  // 紫色
#define 	DSP_PINK            0XFE19  // 粉色
#define 	DSP_RED             0xF800  // 红色
#define 	DSP_MAGENTA         0xF81F  // 品红
#define 	DSP_GREEN           0x07E0  // 绿色
#define 	DSP_CYAN            0x07FF  // 青色
#define 	DSP_YELLOW          0xFFE0  // 黄色
#define 	DSP_BROWN           0XBC40  // 棕色
#define 	DSP_GRAY            0X8430  // 灰色

#define		MIUI_Clear			ips114_clear(DSP_BLACK)
#define		MIUI_ShowString( x, y, fc, bc, s)		GuideChese_DSPFunction( x, y, fc, bc, *s)

typedef enum
{
    //状态标志
    _KeyStatus_Reset = 0,
    _KeyStatus_Chick,
    _KeyStatus_Enable,
    _KeyStatus_Clear,


    //存储参数
    _KeyStore_NULL,
    _KeyStore_KEYUp,
    _KeyStore_KEYDown,
    _KeyStore_KEYLeft,
    _KeyStore_KEYRight,
    _KeyStore_KEYEnable,
    _KeyStore_KEYstorage,

    //选则数据
    _KeySwitch_Open,
    _KeySwitch_Close,
    _KeySwitch_Store,

    //刷新界面
    _MIUIDisplay_Enable,
    _MIUIDisplay_Reset,

    //使能界面
    _MIUIDisplay_Open,
    _MIUIDisplay_Close,

    //使能控制
    _MIUIControl_Enable,
    _MIUIControl_Disable

}__KeyStatus_Enum;


typedef struct __MIUIControl
{
    unsigned char KeyStatus_Present;
    unsigned char KeyStatus_StoreData;
    unsigned char KeyStatus_CurrentTask;
    unsigned char KeyStatus_MIUIDisplay;
    unsigned char MIUIControl_Refresh;
    unsigned char MIUIControl_Enable;

    //长按模式
    unsigned int  MIUIControl_ModelCnt;
    unsigned char MIUIControl_LongData;

}__MIUIControl_Struct;


typedef struct __MIUIDisplay
{
    unsigned char TaskSerial;
    unsigned char SelectData;
    void (*TaskFunction) (void);

}__MIUIDisplay_Struct;


typedef struct __ParamCorrect
{
    char 	*ParamterName;
    float 	*Paramtersprt;
    float 	Stepping;
}__ParamAlter;

typedef struct __ModeCorrect
{
    char 	*ParamterName;
    int 	*Paramtersprt;
    int 	Stepping;
}__ModeAlter;

typedef struct __StoreControl
{
    char 	*ControlName;

}__StoreControl_Struct;


typedef struct __MenuDisplay
{
    unsigned char 	*DSPName;
}__DSPMenu_Struct;



typedef struct __StoreParamFlash
{
    unsigned char Flag_CheckStore;
    unsigned char StoreCountLoop;
}__StoreParamFlash_Struct;



extern __MIUIControl_Struct _MIUIControl;
extern __MIUIDisplay_Struct _MIUIDisplay[MIUINumber];


//--------------外部调用函数----------------
void InitalParameter_MIUIControl(void);
void TaskImplement_MIUIControl  (void);

void BaseConfigure_MIUIFunction (void);
void TaskImplement_MIUIFunction (void);

//---------------底层函数-------------------
//void TaskDisplay_MainFunction (void);
void TaskDisplay_MotorFunction(void);
//void TaskDisplay_ImageFunction(void);
//void TaskDisplay_SteerFunction(void);
//void TaskDisplay_StoreFunction(void);
//void TaskDisplay_OtherFunction(void);




void DisplayPage_MIUIFunction(unsigned char TaskFunction, 
		unsigned char TaskParamNumber, __ModeAlter TaskAlter[]);


#endif





















/*
*2023.08.02
*Fifteen 
*1579985949@qq.com
*/


