





#include "SystemOS.h"





void OS_OperationInital(void)
{
    /*以下是外设初始化*/
	HAL_TIM_Base_Start_IT(&htim2);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_1, 5);
	
    /*ADC/DAC/DMA初始化*/
	WaveInOutputInital();

    /*ISP屏幕初始化*/
	ips114_init();
	
	//人机界面初始化
    InitalParameter_MIUIControl();
//	GuideChese_DSPFunction(30, 0, DSP_GREEN, DSP_BLACK, "Initial OK!");
	
//	printf("Initialization Completed!\n");

}


void OS_OperationSystem(void)
{
	if(_MIUIControl.MIUIControl_Enable == _MIUIControl_Enable)
    {
        _MIUIDisplay[_MIUIControl.KeyStatus_CurrentTask].TaskFunction();
    }
	if(Task_StartFlag)
	{
		WaveInOutputOperation();
	}
	
	HAL_GPIO_TogglePin(GPIOB,GPIO_PIN_6);
}
uint8_t flag = 0;
uint32_t time_1ms = 0, time_10ms = 0;
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim == (&htim2))//1ms
    {
		if(time_1ms++ == 65535)
			time_1ms = 0;
		
		AD9959_Set_Fre(CH0, (WaveFFT_AB[0]*20000)-FreqFix[0]);	//频率(0~200 000 000 Hz)
		AD9959_Set_Fre(CH1, (WaveFFT_AB[1]*20000)-FreqFix[1]);	//频率(0~200 000 000 Hz)
		IO_Update();
		
		WaveSinOrTriOut();
    }
	
    if (htim == (&htim4))//10ms
    {
		TaskImplement_MIUIFunction();
	}
}



































/***
 *               ii.                                         ;9ABH,
 *              SA391,                                    .r9GG35&G
 *              &#ii13Gh;                               i3X31i;:,rB1
 *              iMs,:,i5895,                         .5G91:,:;:s1:8A
 *               33::::,,;5G5,                     ,58Si,,:::,sHX;iH1
 *                Sr.,:;rs13BBX35hh11511h5Shhh5S3GAXS:.,,::,,1AG3i,GG
 *                .G51S511sr;;iiiishS8G89Shsrrsh59S;.,,,,,..5A85Si,h8
 *               :SB9s:,............................,,,.,,,SASh53h,1G.
 *            .r18S;..,,,,,,,,,,,,,,,,,,,,,,,,,,,,,....,,.1H315199,rX,
 *          ;S89s,..,,,,,,,,,,,,,,,,,,,,,,,....,,.......,,,;r1ShS8,;Xi
 *        i55s:.........,,,,,,,,,,,,,,,,.,,,......,.....,,....r9&5.:X1
 *       59;.....,.     .,,,,,,,,,,,...        .............,..:1;.:&s
 *      s8,..;53S5S3s.   .,,,,,,,.,..      i15S5h1:.........,,,..,,:99
 *      93.:39s:rSGB@A;  ..,,,,.....    .SG3hhh9G&BGi..,,,,,,,,,,,,.,83
 *      G5.G8  9#@@@@@X. .,,,,,,.....  iA9,.S&B###@@Mr...,,,,,,,,..,.;Xh
 *      Gs.X8 S@@@@@@@B:..,,,,,,,,,,. rA1 ,A@@@@@@@@@H:........,,,,,,.iX:
 *     ;9. ,8A#@@@@@@#5,.,,,,,,,,,... 9A. 8@@@@@@@@@@M;    ....,,,,,,,,S8
 *     X3    iS8XAHH8s.,,,,,,,,,,...,..58hH@@@@@@@@@Hs       ...,,,,,,,:Gs
 *    r8,        ,,,...,,,,,,,,,,.....  ,h8XABMMHX3r.          .,,,,,,,.rX:
 *   :9, .    .:,..,:;;;::,.,,,,,..          .,,.               ..,,,,,,.59
 *  .Si      ,:.i8HBMMMMMB&5,....                    .            .,,,,,.sMr
 *  SS       :: h@@@@@@@@@@#; .                     ...  .         ..,,,,iM5
 *  91  .    ;:.,1&@@@@@@MXs.                            .          .,,:,:&S
 *  hS ....  .:;,,,i3MMS1;..,..... .  .     ...                     ..,:,.99
 *  ,8; ..... .,:,..,8Ms:;,,,...                                     .,::.83
 *   s&: ....  .sS553B@@HX3s;,.    .,;13h.                            .:::&1
 *    SXr  .  ...;s3G99XA&X88Shss11155hi.                             ,;:h&,
 *     iH8:  . ..   ,;iiii;,::,,,,,.                                 .;irHA
 *      ,8X5;   .     .......                                       ,;iihS8Gi
 *         1831,                                                 .,;irrrrrs&@
 *           ;5A8r.                                            .:;iiiiirrss1H
 *             :X@H3s.......                                .,:;iii;iiiiirsrh
 *              r#h:;,...,,.. .,,:;;;;;:::,...              .:;;;;;;iiiirrss1
 *             ,M8 ..,....,.....,,::::::,,...         .     .,;;;iiiiiirss11h
 *             8B;.,,,,,,,.,.....          .           ..   .:;;;;iirrsss111h
 *            i@5,:::,,,,,,,,.... .                   . .:::;;;;;irrrss111111
 *            9Bi,:,,,,......                        ..r91;;;;;iirrsss1ss1111
 *            这代码是一天也调不下去了！！！！！     明年还打电赛吗？？？？？
 */



/*
*2023.08.02
*Fifteen 
*1579985949@qq.com
*/




































