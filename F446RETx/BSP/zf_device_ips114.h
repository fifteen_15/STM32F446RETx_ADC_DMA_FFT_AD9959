/*********************************************************************************************************************
* CH32V307VCT6 Opensourec Library 即（CH32V307VCT6 开源库）是一个基于官方 SDK 接口的第三方开源库
* Copyright (c) 2022 SEEKFREE 逐飞科技
*
* 本文件是CH32V307VCT6 开源库的一部分
*
* CH32V307VCT6 开源库 是免费软件
* 您可以根据自由软件基金会发布的 GPL（GNU General Public License，即 GNU通用公共许可证）的条款
* 即 GPL 的第3版（即 GPL3.0）或（您选择的）任何后来的版本，重新发布和/或修改它
*
* 本开源库的发布是希望它能发挥作用，但并未对其作任何的保证
* 甚至没有隐含的适销性或适合特定用途的保证
* 更多细节请参见 GPL
*
* 您应该在收到本开源库的同时收到一份 GPL 的副本
* 如果没有，请参阅<https://www.gnu.org/licenses/>
*
* 额外注明：
* 本开源库使用 GPL3.0 开源许可证协议 以上许可申明为译文版本
* 许可申明英文版在 libraries/doc 文件夹下的 GPL3_permission_statement.txt 文件中
* 许可证副本在 libraries 文件夹下 即该文件夹下的 LICENSE 文件
* 欢迎各位使用并传播本程序 但修改内容时必须保留逐飞科技的版权声明（即本声明）
*
* 文件名称          zf_device_ips114
* 公司名称          成都逐飞科技有限公司
* 版本信息          查看 libraries/doc 文件夹内 version 文件 版本说明
* 开发环境          MounRiver Studio V1.8.1
* 适用平台          CH32V307VCT6
* 店铺链接          https://seekfree.taobao.com/
*
* 修改记录
* 日期                                      作者                             备注
* 2022-09-15        大W            first version
********************************************************************************************************************/
/*********************************************************************************************************************
* 接线定义：
*                   ------------------------------------
*                   模块管脚             单片机管脚
*                   SCL                  查看 zf_device_ips114.h 中 IPS114_SCL_PIN 宏定义
*                   SDA                  查看 zf_device_ips114.h 中 IPS114_SDA_PIN 宏定义
*                   RST                  查看 zf_device_ips114.h 中 IPS114_RST_PIN 宏定义
*                   DC                   查看 zf_device_ips114.h 中 IPS114_DC_PIN 宏定义
*                   CS                   查看 zf_device_ips114.h 中 IPS114_CS_PIN 宏定义
*                   BLK                  查看 zf_device_ips114.h 中 IPS114_BLK_PIN 宏定义
*                   VCC                 3.3V电源
*                   GND                 电源地
*                   最大分辨率 135 * 240
*                   ------------------------------------
********************************************************************************************************************/

#ifndef _zf_device_ips114_h_
#define _zf_device_ips114_h_

#include "zf_common_font.h"
#include "gpio.h"
#include "spi.h"

#define IPS114_SOFT_SPI					0										//0-硬件SPI	1-启用软件SPI

#if	IPS114_SOFT_SPI
//====================================================软件 SPI 驱动====================================================
#define IPS114_SOFT_SPI_DELAY           1                                     // 软件 SPI 的时钟延时周期 数值越小 SPI 通信速率越快
#define IPS114_SCL_PIN                  GPIOC,GPIO_PIN_10                     // 软件 SPI SCK 引脚
#define IPS114_SDA_PIN                  GPIOC,GPIO_PIN_12                     // 软件 SPI MOSI 引脚


#define IPS114_SCL(x)                   ((x) ? (HAL_GPIO_WritePin(IPS114_SCL_PIN,GPIO_PIN_SET)) : (HAL_GPIO_WritePin(IPS114_SCL_PIN,GPIO_PIN_RESET)))
#define IPS114_SDA(x)                   ((x) ? (HAL_GPIO_WritePin(IPS114_SDA_PIN,GPIO_PIN_SET)) : (HAL_GPIO_WritePin(IPS114_SDA_PIN,GPIO_PIN_RESET)))

//====================================================软件 SPI 驱动====================================================
#else

#define IPS114_SPI						hspi3									//硬件SPI号

#endif

#define IPS114_RST_PIN                  GPIOA,GPIO_PIN_11                     // 液晶复位引脚定义
#define IPS114_DC_PIN                   GPIOA,GPIO_PIN_12                     // 液晶命令位引脚定义
#define IPS114_CS_PIN                   GPIOA,GPIO_PIN_12                     // CS 片选引脚
#define IPS114_BLK_PIN                  GPIOB,GPIO_PIN_5                      // 液晶背光引脚定义

#define IPS114_DEFAULT_DISPLAY_DIR      (IPS114_CROSSWISE_180)                  // 默认的显示方向
#define IPS114_DEFAULT_PENCOLOR         (RGB565_RED)                            // 默认的画笔颜色
#define IPS114_DEFAULT_BGCOLOR          (RGB565_BLACK)                          // 默认的背景颜色
#define IPS114_DEFAULT_DISPLAY_FONT     (IPS114_8X16_FONT)                      // 默认的字体模式

#define IPS114_DC(x)                    ((x) ? (HAL_GPIO_WritePin(IPS114_DC_PIN,GPIO_PIN_SET)) : (HAL_GPIO_WritePin(IPS114_DC_PIN,GPIO_PIN_RESET)))
#define IPS114_RST(x)                   ((x) ? (HAL_GPIO_WritePin(IPS114_RST_PIN,GPIO_PIN_SET)) : (HAL_GPIO_WritePin(IPS114_RST_PIN,GPIO_PIN_RESET)))
#define IPS114_CS(x)                    ((x) ? (HAL_GPIO_WritePin(IPS114_CS_PIN,GPIO_PIN_SET)) : (HAL_GPIO_WritePin(IPS114_CS_PIN,GPIO_PIN_RESET)))
#define IPS114_BLK(x)                   ((x) ? (HAL_GPIO_WritePin(IPS114_BLK_PIN,GPIO_PIN_SET)) : (HAL_GPIO_WritePin(IPS114_BLK_PIN,GPIO_PIN_RESET)))

typedef enum
{
    IPS114_PORTAIT                      = 0,                                    // 竖屏模式
    IPS114_PORTAIT_180                  = 1,                                    // 竖屏模式  旋转180
    IPS114_CROSSWISE                    = 2,                                    // 横屏模式
    IPS114_CROSSWISE_180                = 3,                                    // 横屏模式  旋转180
}ips114_dir_enum;


extern uint16 ips114_pencolor;
extern uint16 ips114_bgcolor;

extern ips114_dir_enum ips114_display_dir;
extern uint8 ips114_x_max;
extern uint8 ips114_y_max;


void 	ips114_write_8bit_data			(uint8_t dat);
void 	ips114_write_16bit_data			(uint16_t dat);
void    ips114_write_index              (uint8 dat);

void    ips114_clear                    (const uint16 color);
void    ips114_set_dir                  (ips114_dir_enum dir);
void    ips114_set_color                (const uint16 pen, const  uint16 bgcolor);
void    ips114_draw_point               (uint16 x, uint16 y, const uint16 color);
void    ips114_set_region               (uint16 x1, uint16 y1, uint16 x2, uint16 y2);
void    ips114_draw_line                (uint16 x_start, uint16 y_start, uint16 x_end, uint16 y_end, const uint16 color);



void    ips114_draw_point               (uint16 x,uint16 y,uint16 color);
void    ips114_show_char                (uint16 x, uint16 y, const char dat);
void    ips114_show_string              (uint16 x, uint16 y, const char dat[]);
void    ips114_show_int                 (uint16 x,uint16 y, const int32 dat, uint8 num);
void    ips114_show_uint                (uint16 x,uint16 y, const uint32 dat, uint8 num);
void    ips114_show_float               (uint16 x,uint16 y, const float dat, uint8 num, uint8 pointnum);

void    ips114_show_int8                (uint16_t x,uint16_t y,int8_t dat);
void    ips114_show_uint8               (uint16_t x,uint16_t y,uint8_t dat);
void    ips114_show_int16               (uint16_t x,uint16_t y,int16_t dat);
void    ips114_show_uint16              (uint16_t x,uint16_t y,uint16_t dat);
void    ips114_show_int32               (uint16_t x,uint16_t y,int32_t dat,uint8_t num);


void    ips114_show_binary_image        (uint16 x, uint16 y, const uint8 *image, uint16 width, uint16 height, uint16 dis_width, uint16 dis_height);
void    ips114_show_gray_image          (uint16 x, uint16 y, const uint8 *image, uint16 width, uint16 height, uint16 dis_width, uint16 dis_height, uint8 threshold);
void    ips114_show_rgb565_image        (uint16 x, uint16 y, const uint16 *image, uint16 width, uint16 height, uint16 dis_width, uint16 dis_height, uint8 color_mode);

void    ips114_show_wave                (uint16 x, uint16 y, const uint16 *wave, uint16 width, uint16 value_max, uint16 dis_width, uint16 dis_value_max);
void    ips114_show_chinese             (uint16 x, uint16 y, uint8 size, const uint8 *chinese_buffer, uint8 number, const uint16 color);

#define ips114_displayimage7725(p,width,height)                                 ips114_show_binary_image(0,0,p,width,height,width,height)

#define ips114_displayimage032(p,width,height)                                  ips114_show_gray_image(0,0,p,width,height,width,height,0)
#define ips114_displayimage032_zoom(p,width,height,dis_width,dis_height)        ips114_show_gray_image(0,0,p,width,height,dis_width,dis_height,0)
#define ips114_displayimage032_zoom1(p,width,height,x,y,dis_width,dis_height)   ips114_show_gray_image(x,y,p,width,height,dis_width,dis_height,0)

#define ips114_displayimage8660(p,width,height)                                 ips114_show_rgb565_image(0,0,p,width,height,width,height,1)
#define ips114_displayimage8660_zoom(p,width,height,dis_width,dis_height)       ips114_show_rgb565_image(0,0,p,width,height,dis_width,dis_height,1)
#define ips114_displayimage8660_zoom1(p,width,height,x,y,dis_width,dis_height)  ips114_show_rgb565_image(x,y,p,width,height,dis_width,dis_height,1)

void    ips114_init                     (void);

void 	GuideChese_DSPFunction			(uint32_t x, uint32_t y, uint32_t fc, uint32_t bc, const uint8_t *s);

#endif
