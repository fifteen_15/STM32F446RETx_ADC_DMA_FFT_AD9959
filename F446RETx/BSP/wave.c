#include "wave.h"


uint32_t	FreqFix[2] = {0};
int 		FreqOutFixAll = 0;
int 		EverFix[17] = { 21, 26, 32, 37, 42, 47, 53, 57, 63, 68, 73, 78, 83, 89, 94, 99,104};
//int 		EverFix[17] = { 22, 26, 31, 37, 42, 47, 52, 57, 63, 68, 73, 78, 83, 89, 95,101,106};
//int 		EverFix[17] = { 23, 28, 34, 39, 45, 51, 56, 62, 67, 73, 79, 84, 89, 95,101,106,112};

float 		Threshold = 0.0f;					//判断频域的阈值
float 		Va = 0, Vb = 0;						//计算两波幅值
int 		AD9959_Pha_Diff = 0;
uint8_t		Task_StartFlag = 0;
uint8_t		FFT_Complete_Flag = 0;
uint16_t 	RealFreqA = 0,RealFreqB = 0;
uint16_t	FFTFreq[2] = {0};
uint32_t	WaveFFT_AB[4] = {20000, 20000, 0, 0};
uint32_t	WaveFFT_A = 0, WaveFFT_B = 0;


/*FFT全局变量*/
#define 	FFT_LENGTH				4096 		//FFT长度，默认是1024点FFT

arm_cfft_radix4_instance_f32 		scfft;		//定义scfft结构体
float 		FFT_InputBuf[FFT_LENGTH*2];			//FFT输入数组
float 		FFT_OutputBuf[FFT_LENGTH];			//FFT输出数组
uint16_t 	ADC_1_Value_DMA[FFT_LENGTH] = {0};	//存放ADC的值





void WaveInOutputInital(void)
{
	HAL_TIM_Base_Start(&htim3);												//开启TIM3
	HAL_ADC_Start_DMA(&hadc1, (uint32_t *)ADC_1_Value_DMA, FFT_LENGTH);		//开启ADC
	arm_cfft_radix4_init_f32(&scfft,FFT_LENGTH,0,1);						//初始化scfft结构体,设定FFT参数
	
	
	AD9959_Init();
	AD9959_Set_Fre(CH0, (uint32_t)(20000));		//频率(0~200 000 000 Hz)
	AD9959_Set_Fre(CH1, (uint32_t)(20000));		//频率(0~200 000 000 Hz)
	AD9959_Set_Amp(CH0, (uint16_t)1023);		//幅值(0~1023)(0~500mVpp)
	AD9959_Set_Amp(CH1, (uint16_t)1023);		//幅值(0~1023)(0~500mVpp)
	AD9959_Set_Phase(CH0, (uint16_t)0);			//相位(0~16383)(0~360)
	AD9959_Set_Phase(CH1, (uint16_t)0);			//相位(0~16383)(0~360)
	IO_Update();
	
	
}



void WaveInOutputOperation(void)
{

	for(int i=0; i < FFT_LENGTH; i++)
	{
		FFT_InputBuf[2*i]=ADC_1_Value_DMA[i];	//实部
		FFT_InputBuf[2*i+1]=0;					//虚部
	}
	arm_cfft_radix4_f32(&scfft,FFT_InputBuf);					//FFT计算（基4）
	arm_cmplx_mag_f32(FFT_InputBuf,FFT_OutputBuf,FFT_LENGTH);	//取模得幅值
	
	//寻数组内最大值		开始点	寻找范围		最大值	最大值坐标
	arm_max_f32(FFT_OutputBuf+60, 420, &Threshold, &WaveFFT_A);
	if(Threshold < 800000)
		Threshold = 800000;
	
	RealFreqA = 0;
	RealFreqB = 0;
	WaveFFT_A = 0;
	WaveFFT_B = 0;
	for(int i = 60; i < 420; i++)				//频域寻波(15kHz~105kHz)
	{
		if(FFT_OutputBuf[i] > Threshold/2)
		{
			if(RealFreqA < 59)
			{
				RealFreqA = i;
				
				WaveFFT_A = FreqApproximateFive(i);
			}
			else if(RealFreqA > 79)
			{
				RealFreqB = i;
				
				WaveFFT_B = FreqApproximateFive(i);
			}
		}
	}
	
	
	FFTFreq[0] = RealFreqA;
	FFTFreq[1] = RealFreqB;
	WaveFFT_AB[0] = WaveFFT_A;					//题目要求fa<fb
	WaveFFT_AB[1] = WaveFFT_B;
	if(WaveFFT_B == 0)
	{
		FFTFreq[1] = RealFreqA;
		WaveFFT_AB[1] = WaveFFT_A;
	}
	
	WavePhaseFix();								//漂移修正
	
	WaveFFT_AB[2] = 0;
	WaveFFT_AB[3] = 0;
	if(FFTFreq[0] > 59 && FFTFreq[1] > 59)		//三角波检测(三倍频窗口扫描)
	{
		uint16_t ScanRange = 5;					//窗口大小设置
		
		for(int i = FFTFreq[0]*3-ScanRange; i < FFTFreq[0]*3+ScanRange; i++)	//第一个波对应位置扫描
		{
			if(FFT_OutputBuf[i] > Threshold / 23)
			{
				uint16_t Near = FreqApproximateFive(i/3);
				float ExpectV = 0;
				float CurrentV = (FFT_OutputBuf[FFTFreq[0]]*2)/FFT_LENGTH;
				
				if(Near%10 == 0)
				{
					ExpectV = 616 - (FFTFreq[0] - 80) * 0.44;
				}
				else if(Near%10 == 5)
				{
					ExpectV = 423 + (FFTFreq[0] - 80) * 0.49;
				}
				
				Va = ExpectV - CurrentV;
				
				if(Near == WaveFFT_AB[1] && Va < 0)
				{
					WaveFFT_AB[2] = 0;
				}
				else if(Near != WaveFFT_AB[1] && Va < 0)
				{
					WaveFFT_AB[2] = Near;
				}
				else if(Near == WaveFFT_AB[1] && (Va) > 50)
				{
					WaveFFT_AB[2] = Near;
				}
				else if(Near != WaveFFT_AB[1] && (Va) > 35)
				{
					WaveFFT_AB[2] = Near;
				}
			}
		}
		for(int i = FFTFreq[1]*3-ScanRange; i < FFTFreq[1]*3+ScanRange; i++)	//第二个波对应位置扫描
		{
			if(FFT_OutputBuf[i] > Threshold / 23)
			{
				uint16_t Near = FreqApproximateFive(i/3);
				float ExpectV = 0;
				float CurrentV = (FFT_OutputBuf[FFTFreq[1]]*2)/FFT_LENGTH;
				
				if(Near%10 == 0)
				{
					ExpectV = 616 - (FFTFreq[1] - 80) * 0.44;
				}
				else if(Near%10 == 5)
				{
					ExpectV = 423 - (FFTFreq[1] - 80) * 0.49;
				}
				
				Vb = ExpectV - CurrentV;
				
				if(Near == WaveFFT_AB[0] && Vb < 0)
				{
					WaveFFT_AB[3] = 0;
				}
				else if(Near != WaveFFT_AB[0] && Vb < 0)
				{
					WaveFFT_AB[3] = Near;
				}
				else if(Near == WaveFFT_AB[0] && (Vb) > 50)
				{
					WaveFFT_AB[3] = Near;
				}
				else if(Near != WaveFFT_AB[0] && (Vb) > 35)
				{
					WaveFFT_AB[3] = Near;
				}
			}
		}
	}
	
//#if 1			//数据打印上位机分析
//	if(_MIUIControl.MIUIControl_Enable == _MIUIControl_Enable)
//	{
//		printf("%d,%d,%d,%d,%.2f,%.2f\n", WaveFFT_AB[0], WaveFFT_AB[1], WaveFFT_AB[2], WaveFFT_AB[3], Va, Vb);
//	}
//#else
//	for(int i = 55; i < 1260; i++)
//	{
//		printf("%.2f,%d,%d,%d,%d,%d,%d,%d\n", (FFT_OutputBuf[i]*2)/FFT_LENGTH, i, FFTFreq[0], FFTFreq[1], WaveFFT_A, WaveFFT_B, WaveFFT_AB[2], WaveFFT_AB[3]);
//	}
//#endif
}

void WaveSinOrTriOut(void)
{
	//模拟开关使能选择输出正弦波或三角波
	if(WaveFFT_AB[2] == 0)
	{
		CH0_SinOut;
	}
	else
	{
		CH0_TriOut;
	}
	if(WaveFFT_AB[3] == 0)
	{
		CH1_SinOut;
	}
	else
	{
		CH1_TriOut;
	}
}

void WavePhaseFix(void)
{
	//漂移修正
	if(WaveFFT_AB[0] > 15)
	{
		FreqFix[0] = EverFix[(WaveFFT_AB[0]-20)/5] - FreqOutFixAll;
		FreqFix[1] = EverFix[(WaveFFT_AB[1]-20)/5] - FreqOutFixAll;
	}
}

void Task_StartupFunction(int mod)
{
	//模式修改:	( 0 )-参数修改,不进行数据处理以及输出
	//			( 1 )-试运行,不关闭菜单,但是进行数据处理以及输出
	//			(-1 )-运行,关闭菜单,进行数据处理以及输出
	switch(mod)
	{
		case 0:
			Task_StartFlag = 0;
		break;
		case 1:
			Task_StartFlag = Task_StartFlag==0?1:0;
			AD9959_Set_Phase(CH0, (uint16_t)(AD9959_Pha_Diff*45.508f));	//相位(0~16383)(0~360)
			AD9959_Set_Phase(CH1, (uint16_t)(0));	//相位(0~16383)(0~360)
			HAL_GPIO_TogglePin(GPIOB,GPIO_PIN_8);
		break;
		case -1:
			Task_StartFlag = 1;
			AD9959_Set_Phase(CH0, (uint16_t)(AD9959_Pha_Diff*45.508f));	//相位(0~16383)(0~360)
			AD9959_Set_Phase(CH1, (uint16_t)(0));	//相位(0~16383)(0~360)
			_MIUIControl.MIUIControl_Enable = _MIUIControl_Disable;
			HAL_GPIO_TogglePin(GPIOB,GPIO_PIN_7);
		break;
		default:
			break;
	}
	
}

uint16_t FreqApproximateFive(uint16_t num)
{
	//频率5kHz近似
	uint16_t Near = 0;
	uint16_t Quotient  = (uint16_t)(num)/(FFT_LENGTH/1024) / 5;	//求商
	uint16_t Remainder = (uint16_t)(num)/(FFT_LENGTH/1024) % 5;	//求余
	
	if(Remainder > 2.5)
	{
		Near = Quotient * 5 + 5;
	}
	else
	{
		Near = Quotient * 5;
	}
	
	return Near;
}



















/*
*2023.08.02
*Fifteen 
*1579985949@qq.com
*/











